public class ShopFullException extends Exception{
    public ShopFullException(String s) {
        super(s);
    }
}
