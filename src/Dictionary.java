import java.util.HashMap;

public class Dictionary {
    HashMap<String, String>  words;

    public Dictionary(HashMap<String, String> words) {
        this.words = words;
    }

    public static void doMagic(){
        Dictionary dic=new Dictionary(new HashMap<>());

        dic.addWord("kot", "cat");
        dic.addWord("kotek", "kitten");
        dic.addWord("pies", "pies");
        System.out.println("dic.editWord(\"pies\", \"dog\") = " + dic.editWord("pies", "dog"));
        System.out.println("dic.editWord(\"rybka\", \"fish\") = " + dic.editWord("rybka", "fish"));

        dic.print();
        System.out.println("\n------------------\n");
        dic.seach("kot");

        System.out.println("\n------------------\n");
        dic.seach("k");
    }

    public void addWord(String key, String value){
        words.put(key, value);
    }

    public void print(){
        for(String key: words.keySet()){
            System.out.println("key+\" - \"+words.get(key) = " + key + " - " + words.get(key));
        }
    }

    public boolean editWord(String key, String value){
        boolean existed=false;
        if(words.get(key)!=null)existed=true;

        words.put(key, value);

        return existed;
    }

    public void seach(String needle){
        for(String key: words.keySet()){
            if((key.toUpperCase()).contains(needle.toUpperCase()))
            System.out.println("key+\" - \"+words.get(key) = " + key + " - " + words.get(key));
        }
    }
}
