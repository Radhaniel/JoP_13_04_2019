import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class PersonManager {
    private static List<Person> personList=new ArrayList<Person>();

    public static void doMagic() {
        for (int it = 0; it < 10; it++) {
            personList.add(new Person("Imie" + it, "Nazwaiko" + it, 18 + it));

        }
        System.out.println("personList.get(0) = " + personList.get(0));
        personList.remove(0);

        Person tmp=new Person("Michał", "Frankowski", 30);
        personList.add(tmp);

        System.out.println("personList.get(0) = " + personList.get(0));

        System.out.println("personList.get(personList.size()) = " + personList.get(personList.size()-1));



        System.out.println("personList.contains(tmp) = " + personList.contains(tmp));

        System.out.println("personList.size() = " + personList.size());
    }

}
