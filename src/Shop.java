

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Shop {
    private Queue<Person> myQueue;

    public static void doMagic(){
        Shop myShop=new Shop();

        ArrayList<Person> street=new ArrayList<>();
        for (int it = 0; it < 30; it++) {
            street.add(new Person("Imie" + it, "Nazwaiko" + it, 18 + it));
        }

        do{
            for(int it=0; it<2; it++) {
                if(!street.isEmpty()) {
                    Person tmp = street.get(0);
                    street.remove(0);
                    try {
                        System.out.println(tmp + " wchodzi do sklepu");
                        myShop.joinQueue(tmp);
                    } catch (ShopFullException ex) {
                        street.add(tmp);
                        System.out.println(ex.getMessage());
                    }
                    ;
                }
            }
            System.out.println("Kolejna osoba: " + myShop.getMyQueue().peek());
            myShop.serve();

        }while(!myShop.isEmpty());
    }

    private Shop(){
        init();
    }

    private void init(){
        setMyQueue(new LinkedList<Person>());
    }

    private Queue getMyQueue() {
        return myQueue;
    }

    private void setMyQueue(LinkedList<Person> myQueue) {
        this.myQueue =  myQueue;
    }

    public void joinQueue(Person person) throws ShopFullException{

        if(getMyQueue().size()>=5)throw new ShopFullException("Sklep jest pełny, "+person+" nie wejdzie!");
        else getMyQueue().add(person);
    }

    private void serve(){
        System.out.println("Obsługuję " + getMyQueue().poll());
    }

    private boolean isEmpty(){
        return getMyQueue().isEmpty();
    }
}
