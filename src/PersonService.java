import java.util.HashMap;

public class PersonService {
    HashMap<Integer, Person> DB;
    private static int sequence=1;


    public PersonService() {
        this.DB = new HashMap<>();
    }

    public static void doMagic(){
        PersonService list=new PersonService();

        for (int it = 0; it < 10; it++) {
            list.addPerson(new Person("Imie" + it, "Nazwaiko" + it, 18 + it));

        }

        list.print();
    }

    public void addPerson(Person pr){
        DB.put(sequence, pr);
        sequence++;
    }
    public void print(){
        for(int it:DB.keySet()){
            System.out.println(it + " " + DB.get(it));
        }
    }
}
