import java.util.HashSet;

public class MyCountries {
    private static HashSet<Country> mySet=new HashSet<>();

    public static void doMagic(){

        addContry(new Country("Polska"));
        addContry(new Country("Niemcy"));
        addContry(new Country("Anglia"));
        addContry(new Country("Polska"));
        addContry(new Country("Rosja"));

        getAllCountreis();
    }

    private static void addContry(Country country){
        mySet.add(country);
    }

    private static void getAllCountreis(){
        for(Country ctr:mySet){
            System.out.println(ctr);
        }

        //return "";
    }
}
