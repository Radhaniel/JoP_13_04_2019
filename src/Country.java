public class Country {
    String name;

    public Country(String name) {
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getName()+"-"+this.hashCode();
    }
}
