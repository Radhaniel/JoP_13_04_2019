import java.math.BigDecimal;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
	// write your code here
        //zadanie1();
        //zadanie2();

        //PersonManager.doMagic();


        //MyCountries.doMagic();
        //PersonService.doMagic();
        //System.out.println("\n<----->");
        //Dictionary.doMagic();

        Shop.doMagic();
    }

    private static void zadanie2(){
        int[] mainTab=new int[16];
        initRandArrray(mainTab);
        printArray(mainTab);
        int needle=mainTab[0];
        bubleSort(mainTab);
        System.out.println("\n<----->");
        printArray(mainTab);

        System.out.println("needle = " + needle);
        int p=0, k=mainTab.length-1;

        System.out.println("binarySearch(mainTab, needle, p, k) = " + binarySearch(mainTab, needle, p, k));
    }

    private static int binarySearch(int[] tab, int needle, int p, int k){
        int s;


        s=p+Math.round((k-p)/2);



        if(tab[s]==needle)return s;
        else if(tab[s]>needle){
            return binarySearch(tab, needle, p, s);
        }else{
            return binarySearch(tab, needle, s, k);
        }
    }

    private static void bubleSort(int[] tab){
        boolean wasMoved=false;
        int tmp;
        do{
            wasMoved=false;
            for(int it=1; it<tab.length; it++){
                if(tab[it-1]>tab[it]){
                    tmp=tab[it];
                    tab[it]=tab[it-1];
                    tab[it-1]=tmp;
                    wasMoved=true;
                }
            }
        }while(wasMoved);
    }

    private static void zadanie1(){
        int[] mainTab=new int[10];
        initArrray(mainTab);
        //printArray(mainTab);

        for(int it=0; it<mainTab.length; it++){
            if(it%2==1)mainTab[it]+=mainTab[it-1];
        }
        //printArray(mainTab);

        //initArrray(mainTab);
        for(int it=0; it<mainTab.length; it++){
            if(mainTab[it]%2==0)mainTab[it]/=2;
        }
        //printArray(mainTab);

        int suma=0;
        BigDecimal sumaBig= BigDecimal.valueOf(0);

        for(int it=0; it<mainTab.length; it++){
            suma+=mainTab[it];
            //624250
            //624999250000
            //62499992500000
            //6249999925000000
            sumaBig=sumaBig.add(BigDecimal.valueOf(mainTab[it]));
        }
        System.out.println("suma = " + sumaBig);
    }

    private static void printArray(int[] tab){
        for(int it=0; it<tab.length; it++){
            //if(it%9==0)System.out.println(tab[it]);
            //else
                System.out.print(tab[it]+ " ");
        }
    }

    private static void initArrray(int[] tab){
        for(int it=0; it<tab.length; it++){
            tab[it]=it;
        }
    }

    private static void initRandArrray(int[] tab){
        Random rand=new Random(30);

        for(int it=0; it<tab.length; it++){
            tab[it]=Math.abs(rand.nextInt(20));
        }
    }
}
